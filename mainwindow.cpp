#include "mainwindow.h"

#include <iostream>
#include <algorithm>
#include "saveitemstofile.h"

MainWindow::MainWindow()
    : box(Gtk::ORIENTATION_VERTICAL, 8),
      label("TreeView Sample")
{
  set_title("TreeView Sample");
  set_border_width(8);
  set_default_size(650, 400);

  add(box);
  box.pack_start(label, Gtk::PACK_SHRINK);

  scrolledWindow.set_shadow_type(Gtk::SHADOW_ETCHED_IN);
  scrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
  box.pack_start(scrolledWindow);

  create_model();

  /* create tree view */
  treeView.set_model(treeStore);
  Glib::RefPtr<Gtk::TreeSelection> refTreeSelection = treeView.get_selection();
  refTreeSelection->set_mode(Gtk::SELECTION_MULTIPLE);
  add_columns();

  scrolledWindow.add(treeView);

  show_all();
  show_all_children();

  treeView.collapse_all();

  treeView.signal_row_expanded().connect(sigc::mem_fun(*this,
                                                       &MainWindow::treeview_row_expanded_handler));
  treeView.signal_row_activated().connect(sigc::mem_fun(*this,
                                                        &MainWindow::on_my_row_activated));
  treeView.set_activate_on_single_click(true);

  signal_delete_event().connect(sigc::mem_fun(*this,
                                              &MainWindow::on_delete_event_handler));
}

bool MainWindow::on_delete_event_handler(GdkEventAny *event)
{
  SaveItemsToFile(getParentItemsForSave(), "items.json").save();
  return false;
}

std::vector<Item> MainWindow::getParentItemsForSave()
{
  std::vector<Item> items;

  for (auto element : treeRowsByItems)
  {
    if (element.item.hasParent() == false)
    {
      if (element.item.hasChildren &&  element.item.children.empty()) {
          element.item.children = LoadItemsByFile("items.json").getChildrenItemsById(element.item.id);
      }

      items.push_back(element.item);
    }
  }

  return items;
}

void MainWindow::treeview_row_expanded_handler(const Gtk::TreeModel::iterator &iter, const Gtk::TreeModel::Path &path)
{
  if (itemNeedLoadChildren(iter) == true)
  {
    treeStore->erase(iter->children().begin());

    loadChildrenItems(iter);

    treeView.expand_to_path(path);
  }
}

void MainWindow::on_my_row_activated(const Gtk::TreeModel::Path &path, Gtk::TreeViewColumn *column)
{
  const auto iter = treeStore->get_iter(path);

  if (iter)
  {
    const Gtk::TreeRow row = *iter;
    manageChosenBehavior(row);
  }
}

void MainWindow::manageChosenBehavior(const Gtk::TreeRow &row)
{
  bool state = row[modelColumns.isActive];

  TreeRowListIterator element = std::find_if(treeRowsByItems.begin(), treeRowsByItems.end(), [=](TreeRowByItem &it)
                                             { return it.row == row; });
  (*element).item.isActive = state;

  if ((*element).item.hasParent() == true)
  {
    auto parent = std::find_if(treeRowsByItems.begin(), treeRowsByItems.end(), [&](TreeRowByItem it)
                               { return it.item.id == (*element).item.parentId; });

    checkInParentItemsList(parent, element, state);
    checkParentIfNeed(parent);
  }
  else
  {
    checkChildrenElements(element, state);
  }
}

void MainWindow::checkInParentItemsList(TreeRowListIterator &parent, TreeRowListIterator &element, bool state)
{

  auto elementInChild = std::find_if((*parent).item.children.begin(), (*parent).item.children.end(), [&](Item it)
                                     { return it.id == (*element).item.id; });

  (*elementInChild).isActive = state;
}

void MainWindow::checkParentIfNeed(TreeRowListIterator &parent)
{
  bool childIsActive = true;

  auto children = (*parent).item.children;
  auto childWithAnotherState = std::find_if(children.begin(), children.end(), [&](Item it)
                                            { return it.isActive != childIsActive; });

  bool parentIsActive = childWithAnotherState == children.end();

  (*parent).item.isActive = parentIsActive;
  (*parent).row[modelColumns.isActive] = parentIsActive;
}

void MainWindow::checkChildrenElements(TreeRowListIterator &element, bool state)
{

  if ((*element).item.children.empty() == false)
  {
    for (auto &child : (*element).item.children)
    {
      child.isActive = state;
    }
    for (auto child : (*element).row.children())
    {
      child[modelColumns.isActive] = state;
    }
  }
}

void MainWindow::loadChildrenItems(const Gtk::TreeModel::iterator &iter)
{
  auto item = std::find_if(treeRowsByItems.begin(), treeRowsByItems.end(), [=](TreeRowByItem it)
                           { return it.row == (*iter); });
  ;

  (*item).item.children = LoadItemsByFile("items.json").getChildrenItemsById((*item).item.id);

  for (auto child : (*item).item.children)
  {
    Gtk::TreeRow child_row = *(treeStore->append((*iter).children()));
    child_row[modelColumns.label] = child.label;
    child_row[modelColumns.isActive] = child.isActive;

    treeRowsByItems.push_back(TreeRowByItem(child_row, child));
  }
}

bool MainWindow::itemNeedLoadChildren(const Gtk::TreeModel::iterator &iter)
{
  auto item = std::find_if(treeRowsByItems.begin(), treeRowsByItems.end(), [=](TreeRowByItem it)
                           { return it.row == (*iter); });
  ;

  if (item != treeRowsByItems.end())
  {
    return (*item).item.hasChildren && (*item).item.children.empty();
  }

  return false;
}

void MainWindow::create_model()
{
  treeStore = Gtk::TreeStore::create(modelColumns);

  auto items = add_items();

  std::for_each(
      items.begin(), items.end(),
      sigc::mem_fun(*this, &MainWindow::treestore_add_item));
}

std::vector<Item> MainWindow::add_items()
{
  return LoadItemsByFile("items.json").getParentItems();
}

void MainWindow::treestore_add_item(const Item &foo)
{

  Gtk::TreeRow row = *(treeStore->append());

  row[modelColumns.label] = foo.label;
  row[modelColumns.isActive] = foo.isActive;

  if (foo.hasChildren == true && foo.children.empty() == true)
  {
    Gtk::TreeRow child_row = *(treeStore->append(row.children()));
  }

  treeRowsByItems.push_back(TreeRowByItem(row, foo));
}

void MainWindow::add_columns()
{
  {
    int cols_count = treeView.append_column_editable("Active", modelColumns.isActive);
    Gtk::TreeViewColumn *pColumn = treeView.get_column(cols_count - 1);
    if (pColumn)
    {

      Gtk::CellRenderer *pRenderer = pColumn->get_first_cell();
      pRenderer->property_xalign().set_value(0.0);

      pColumn->set_clickable();
    }
  }
  {
    int cols_count = treeView.append_column("Label", modelColumns.label);
    Gtk::TreeViewColumn *pColumn = treeView.get_column(cols_count - 1);
    if (pColumn)
    {
      Gtk::CellRenderer *pRenderer = pColumn->get_first_cell();
      pRenderer->property_xalign().set_value(0.0);

      pColumn->set_clickable();
    }
  }
}