#pragma once

#include "structs.h"
#include <nlohmann/json.hpp>

using nlohmann::json;

class SaveItemsToFile
{
public:
    SaveItemsToFile(std::vector<Item> itemsList, std::string path);
    void save();
private:
    std::vector<Item> items;
    std::string path;

    json getJson();
};