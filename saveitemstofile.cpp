#include "saveitemstofile.h"
#include <iostream>
#include <fstream> 

SaveItemsToFile::SaveItemsToFile(std::vector<Item> itemsList, std::string path) : items(itemsList),
                                                                                  path(path)
{
}

void SaveItemsToFile::save()
{
    std::ofstream myfile(path);
    myfile << getJson().dump();
    myfile.close();
}

json SaveItemsToFile::getJson()
{
    json j;
    j["items"] = json::array();

    for (auto item : items)
    {
        json jsonItemObject = json::object();

        jsonItemObject["label"] = item.label;
        jsonItemObject["id"] = item.id;
        jsonItemObject["parentId"] = item.parentId;
        jsonItemObject["checked"] = item.isActive;

        if (item.children.empty() == false)
        {
            jsonItemObject["children"] = json::array();

            for (auto child : item.children)
            {
                json jsonChildItemObject = json::object();

                jsonChildItemObject["label"] = child.label;
                jsonChildItemObject["id"] = child.id;
                jsonChildItemObject["parentId"] = child.parentId;
                jsonChildItemObject["checked"] = child.isActive;

                jsonItemObject["children"].push_back(jsonChildItemObject);
            }
        }

        j["items"].push_back(jsonItemObject);
    }

    return j;
}