#pragma once

#include <string>
#include <vector>

struct Item
{
    std::string label;
    bool isActive;
    std::vector<Item> children;
    bool hasChildren = false;
    int id;
    int parentId =- 1;

    Item(std::string label, bool isActive, int id) : label(label),
                                                     isActive(isActive),
                                                     id(id)
    {
    }

    Item(){};

    bool hasParent()
    {
        return parentId != -1;
    }
};
