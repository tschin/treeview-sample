#pragma once

#include "structs.h"
#include <nlohmann/json.hpp>

using nlohmann::json;

class LoadItemsByFile
{
public:
    LoadItemsByFile(std::string pathToFile);

    std::vector<Item> getParentItems();
    std::vector<Item> getChildrenItemsById(int id);

private:
    std::string path;
    std::string fileBody;
    json jsonItems;

    void parseJson();
    void loadFileBody();
    json findItemById(int id);
    Item getItem(json &jsonItem);
    std::vector<Item> getItemsList(json &jsonItems);
    bool itemHasChildren(json &item);
    bool jsonHasItems(json &item);
};