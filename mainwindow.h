#pragma once

#include <gtkmm.h>
#include "loaditemsbyfile.h"
#include "structs.h"

struct TreeRowByItem
{
    Gtk::TreeRow row;
    Item item;

    TreeRowByItem(Gtk::TreeRow r, Item i)
    {
        row = r;
        item = i;
    }
};

typedef std::vector<TreeRowByItem>::iterator TreeRowListIterator;

class MainWindow : public Gtk::Window
{
public:
    MainWindow();

protected:
    void treeview_row_expanded_handler(const Gtk::TreeModel::iterator& iter, const Gtk::TreeModel::Path& path);
    virtual void create_model();
    virtual void add_columns();
    virtual std::vector<Item> add_items();
    virtual void treestore_add_item(const Item &foo);
    virtual void on_my_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column);
    bool on_delete_event_handler(GdkEventAny* event);


    void loadChildrenItems(const Gtk::TreeModel::iterator &iter);
    bool itemNeedLoadChildren(const Gtk::TreeModel::iterator &iter);
    TreeRowListIterator getTreeRowByItemElement(const Gtk::TreeModel::iterator &iter);
    void checkChildrenElements(TreeRowListIterator &element, bool state);
    void checkParentIfNeed(TreeRowListIterator &parent);
    void checkInParentItemsList(TreeRowListIterator &parent, TreeRowListIterator &element, bool state);
    void manageChosenBehavior(const Gtk::TreeRow &row);
    std::vector<Item> getParentItemsForSave();

    //Member widgets:
    Gtk::Box box;
    Gtk::ScrolledWindow scrolledWindow;
    Gtk::Label label;
    Gtk::TreeView treeView;
    Glib::RefPtr<Gtk::TreeStore> treeStore;
    std::vector<TreeRowByItem> treeRowsByItems;
    struct ModelColumns : public Gtk::TreeModelColumnRecord
    {
        Gtk::TreeModelColumn<Glib::ustring> label;
        Gtk::TreeModelColumn<bool> isActive;

        ModelColumns()
        {
            add(label);
            add(isActive);
        };
    };

    const ModelColumns modelColumns;
};
