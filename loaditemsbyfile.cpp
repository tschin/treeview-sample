#include "loaditemsbyfile.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>

LoadItemsByFile::LoadItemsByFile(std::string pathToFile)
    : path(pathToFile)
{

    loadFileBody();
    parseJson();
}

void LoadItemsByFile::parseJson()
{
    jsonItems = json::parse(fileBody);
}

Item LoadItemsByFile::getItem(json &jsonItem)
{
    Item item(jsonItem["label"],
              jsonItem["checked"],
              jsonItem["id"]);

    item.hasChildren = itemHasChildren(jsonItem);
    return item;
}

std::vector<Item> LoadItemsByFile::getParentItems()
{
    std::vector<Item> items;

    if (jsonHasItems(jsonItems))
    {
        items = getItemsList(jsonItems["items"]);
    }

    return items;
}

std::vector<Item> LoadItemsByFile::getItemsList(json &jsonItems)
{
    std::vector<Item> items;

    for (auto jsonItem : jsonItems)
    {
        items.push_back(getItem(jsonItem));
    }

    return items;
}

std::vector<Item> LoadItemsByFile::getChildrenItemsById(int id)
{
    std::vector<Item> items;

    json jsonItem = findItemById(id);

    if (itemHasChildren(jsonItem) == true)
    {
        items = getItemsList(jsonItem["children"]);

        for(auto &item: items) {
            item.parentId = id;
        }
    }
    
    return items;
}

bool LoadItemsByFile::jsonHasItems(json &item)
{
    return jsonItems.contains("items") && jsonItems["items"].size() > 0;
}

bool LoadItemsByFile::itemHasChildren(json &item)
{
    return item.contains("children") && item["children"].size() > 0;
}

json LoadItemsByFile::findItemById(int id)
{
    json item;
    for (auto jsonItem : jsonItems["items"])
    {
        if (jsonItem["id"] == id)
        {
            item = jsonItem;
            break;
        }
    }

    return item;
}

void LoadItemsByFile::loadFileBody()
{
    std::string line;
    std::ifstream myfile(path);
    if (myfile.is_open())
    {
        while (getline(myfile, line))
        {
            fileBody.append(line);
        }

        myfile.close();
    }
}